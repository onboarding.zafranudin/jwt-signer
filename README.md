### JWT Signer

Simple usage:

```
$ npm i
$ node index.js --token=your_token_from_service --userId='1'
```
#### Available options
| options                   | value                                 |
| -------------------       | ---                                   |
| --token                   | [String] Your token                   |
| --userId                  | [String] User ID that will be used    |
| --expiresIn (optional)    | [Number] expires in minutes           |
| --type (optional)         | [String] The type of user you are     |

Feel free to contribute!
