const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv
const jsonwebtoken = require('jsonwebtoken')

const { userId, type, token, expiresIn } = argv;

if (!token || !userId) throw new Error("Require --token and --userId");
if (Number.isNaN(expiresIn)) throw new Error("`expiresIn should be number, representing minutes");

const authPayload = {
    userId,
    type: type || 'service',
}

const signed = jsonwebtoken.sign(authPayload, token, {
    expiresIn: `${Number(expiresIn) || 3000} minutes`,
});

console.log(signed);
